from ulauncher.api.client.Extension import Extension
from ulauncher.api.client.EventListener import EventListener
from ulauncher.api.shared.event import *
from ulauncher.api.shared.item.ExtensionResultItem import ExtensionResultItem
from ulauncher.api.shared.action.RenderResultListAction import RenderResultListAction
from ulauncher.api.shared.action.HideWindowAction import HideWindowAction


from clipboard import Clip

clip = Clip()

N_LIMIT = 8

class PreferencesUpdateEventListener(EventListener):
    def on_event(self, event, extension):
        if event.id == 'limit':
            global N_LIMIT
            N_LIMIT = event.new_value

class PreferencesEventListener(EventListener):
    def on_event(self, event, extension):
        global N_LIMIT
        N_LIMIT = int(event.preferences['limit'])

class DemoExtension(Extension):

    def __init__(self):
        super(DemoExtension, self).__init__()
        self.subscribe(KeywordQueryEvent, KeywordQueryEventListener())
        self.subscribe(PreferencesEvent, PreferencesEventListener())
        self.subscribe(PreferencesUpdateEvent, PreferencesUpdateEventListener())


class KeywordQueryEventListener(EventListener):
    def on_event(self, event, extension):
        clips = clip.get_clips(N_LIMIT)
        # subprocess.check_output(["/usr/sbin/clipster","-o","-n",str(N_LIMIT),"-0"]).split('\0')
        items = []
        for c in clips:
	        items.append(ExtensionResultItem(icon='images/icon.png',
                                             name=c,
                                             description=c,
                                             on_enter=HideWindowAction()))

        return RenderResultListAction(items)
    
    def on_event_2(self, event, extension):
        items = sh.clipster('-o', '-n', str(N_LIMIT), '-0').split('\0')
        for i in items:
            items.append(ExtensionResultItem(icon='images/icon.png',
                                             name=i.split('\n')[0],
                                             description=i,
                                             on_enter=HideWindowAction()))

        return RenderResultListAction(items)

if __name__ == '__main__':
    DemoExtension().run()
